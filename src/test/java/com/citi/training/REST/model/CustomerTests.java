package com.citi.training.REST.model;

import static org.junit.Assert.*;

import org.junit.Test;

import com.citi.training.customers.model.Customer;

public class CustomerTests {

    private int testId = 3;
    private String testName = "Maeve";
    private String testAddress = "123 stupid lane";

    @Test
    public void test_Product_constructor() {
        Customer testCustomer = new Customer(testId, testName, testAddress);;

        assertEquals(testId, testCustomer.getId());
        assertEquals(testName, testCustomer.getName());
        assertEquals(testAddress, testCustomer.getAddress());
    }

    @Test
    public void test_Product_toString() {
        String testString = new Customer(testId, testName, testAddress).toString();

        assertTrue(testString.contains((new Integer(testId)).toString()));
        assertTrue(testString.contains(testName));
        assertTrue(testString.contains((testAddress)));
    }

}
