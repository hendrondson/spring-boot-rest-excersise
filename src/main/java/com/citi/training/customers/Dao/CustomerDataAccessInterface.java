package com.citi.training.customers.Dao;

import java.util.List;

import com.citi.training.customers.model.Customer;

public interface CustomerDataAccessInterface {
	
	void createCustomer(Customer customer);
	
	List<Customer> getAllCustomers();
	
	Customer getCustomer(int id);
	
	void deleteCustomer(int id); 
	
}
