package com.citi.training.customers.Dao;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.stereotype.Component;

import com.citi.training.customers.exceptions.CustomerNotFoundException;
import com.citi.training.customers.model.Customer;

@Component
public class InMemCustomerDao implements CustomerDataAccessInterface {

	private static Logger LOG = LoggerFactory.getLogger(InMemCustomerDao.class);

	private Map<Integer, Customer> allCustomers = new HashMap<Integer, Customer>();

	@Override
	public void createCustomer(Customer customer) {
		allCustomers.put(customer.getId(), customer);

	}

	@Override
	public List<Customer> getAllCustomers() {
		return new ArrayList<Customer>(allCustomers.values());

	}

	@Override
	public Customer getCustomer(int id) {
		Customer foundCustomer = allCustomers.get(id);

		LOG.info("FoundCustomer: " + foundCustomer);

		if (foundCustomer == null) {
			throw new CustomerNotFoundException("Product not found " + id);
		}
		return foundCustomer;

	}

	@Override
	public void deleteCustomer(int id) {

		Customer removeProduct = allCustomers.remove(id);

		if (removeProduct == null) {
			throw new CustomerNotFoundException("Product not found " + id);

		}
	}
}
