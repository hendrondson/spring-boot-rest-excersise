package com.citi.training.customers.rest;

import java.util.List;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpEntity;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;
import org.springframework.web.bind.annotation.ResponseStatus;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.http.MediaType;

import com.citi.training.customers.Dao.CustomerDataAccessInterface;
import com.citi.training.customers.model.Customer;

@RestController
@RequestMapping("/customer")
public class RESTController {

	private static Logger LOG = LoggerFactory.getLogger(RESTController.class);
	@Autowired
	private CustomerDataAccessInterface customerRepository;

	@RequestMapping(method = RequestMethod.GET)
	public List<Customer> getAll() {
		LOG.info("getALL was called");
		LOG.debug("this is a debug message");
		return customerRepository.getAllCustomers();
	}

	@RequestMapping(method = RequestMethod.POST, produces = MediaType.APPLICATION_JSON_VALUE, consumes = MediaType.APPLICATION_JSON_VALUE)
	public HttpEntity<Customer> createCustomer(@RequestBody Customer customer) {
		LOG.info("saveProduct was called: " + customer);
		customerRepository.createCustomer(customer);
		return new ResponseEntity<Customer>(customer, HttpStatus.CREATED);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.GET, produces = MediaType.APPLICATION_JSON_VALUE)
	public Customer getCustomer(@PathVariable int id) {
		LOG.info("getProduct was called: " + id);
		return customerRepository.getCustomer(id);
	}

	@RequestMapping(value = "/{id}", method = RequestMethod.DELETE)
	@ResponseStatus(value = HttpStatus.NO_CONTENT)
	public void deleteProduct(@PathVariable int id) {
		LOG.info("deleteProduct was called: " + id);
		customerRepository.deleteCustomer(id);
	}
}
