package com.citi.training.customers.exceptions;

@SuppressWarnings("serial")
public class CustomerNotFoundException extends RuntimeException {
	
	public CustomerNotFoundException(String errorString) {
		super(errorString);
	}

}
