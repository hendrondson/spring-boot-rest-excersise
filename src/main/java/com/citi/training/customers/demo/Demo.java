package com.citi.training.customers.demo;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.ApplicationArguments;
import org.springframework.stereotype.Component;

import com.citi.training.customers.Dao.CustomerDataAccessInterface;
import com.citi.training.customers.model.Customer;

@Component
public class Demo {

	@Autowired
	private CustomerDataAccessInterface customerRepository;

	public void run(ApplicationArguments appArgs) {
		System.out.println("Creating some customers");
		String[] customerNames = { "Beans", "James", "Lucy" };
		String[] customerAddresses = { "123 lovely lane", "25 stupid lane", "19 ohh mummy" };

		for (int i = 0; i < customerNames.length; ++i) {
			Customer thisCustomer = new Customer(i, customerNames[i], customerAddresses[i]);

			System.out.println("Created Customer: " + thisCustomer);

			customerRepository.createCustomer(thisCustomer);
		}

		System.out.println("\nAll Products:");
		for (Customer customer : customerRepository.getAllCustomers()) {
			System.out.println(customer);
		}

		System.out.println("Demo Finished - Exiting");
	}

}
